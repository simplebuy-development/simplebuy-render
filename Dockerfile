FROM node
MAINTAINER Alexey Malafeev <malafeev.alexey@gmail.com>
# Make source directory
RUN mkdir src
WORKDIR src
# Install NPM dependencies
COPY ./package.json ./package.json
RUN npm config set prefix $PWD && npm install && npm prune
# Update PATH environment variable to be able to use local dependencies as global
ENV PATH ./node_modules/.bin:$PATH
# Copy all other files to source directory
COPY ./ .
# Expose html storage
VOLUME /html
# Expose api server
EXPOSE 4000
# Start stub server by default
CMD ["npm", "start"]
