var phantom = require('phantom');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var fs = require('fs');
var URL = require('url');
var path = require('path');
var cheerio = require('cheerio');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const RENDERED_PAGES_DIR = '/rendered_pages';

function ensureDirectoryExistence(filePath) {
  // Check directory path existence and create it if necessary
  var dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}

// POST /render {url: http://abc.com, -  mandatory parameter
//               keywords: 'bla bla bla bla bla' - optional parameter
//              }
app.post('/render', function (req, res) {
  if (!req.body.url) {
    res.status(400).send('Please specify full URL in the request');
  }

  var url = req.body.url;
  var keywords = req.body.keywords;
  console.log('Render url: ' + url + ' keywords: ' + keywords);

  // Open provided URL in the PhantomJS headless browser
  phantom.create().then(function(ph) {
    ph.createPage().then(function(page) {
      page.open(url).then(function(status) {
        // Get page content(html)
        page.property('content').then(function(content) {
          page.close(); // Close page
          ph.exit(); // Exit browser

          // Parse URL with nodeJS url parser, please see reference here:
          // https://nodejs.org/docs/latest/api/url.html
          var parsedUrl = URL.parse(url);

          // Create file path with format like this - 
          //  html/hostname/subfolder/page.html
          // For example, we need to render url -
          //  abc.com/products/123
          // It will be rendered to html/abc.com/products/123.html
          var filepath = parsedUrl.hostname + '/' + 
                         parsedUrl.pathname + '.html';
          if (parsedUrl.pathname === '/'){
            filepath = parsedUrl.hostname + '/' + parsedUrl.pathname + 
                       'index.html';
          }

          var fullpath = path.join(RENDERED_PAGES_DIR, filepath);
          ensureDirectoryExistence(fullpath);

          if (keywords){
            // Write keywords into <meta name="description" content="">
            var $ = cheerio.load(content);
            $('meta[name="description"]').attr('content', keywords);
            content = $.html();
          }
          console.log('Write to ' + fullpath);
          fs.writeFile(fullpath, content, function(err) {
            if(err) {
              res.status(500).send(err);
              return;
            }
            res.status(200).send('OK');
          }); 
        });
      });

    });
  });
  
});

// Get rendered page /?url=http://abc.com
app.get('*', function (req, res) {
  var url = req.query.url;
  console.log('Getting ' + req.query.url);

  var parsedUrl = URL.parse(url);
  var fullpath;
  if (parsedUrl.pathname === '/'){
    fullpath = path.join(RENDERED_PAGES_DIR, parsedUrl.hostname + 
               '/' + parsedUrl.pathname + 'index.html'); 
  }
  else if (parsedUrl.pathname === '/robots.txt'){
    fullpath = path.join(__dirname, parsedUrl.pathname);
  }
  else {
    fullpath = path.join(RENDERED_PAGES_DIR, parsedUrl.hostname + 
               '/' + parsedUrl.pathname + '.html'); 
  }

  console.log('Return rendered page ' + fullpath);
  if (fs.existsSync(fullpath)) {
      
    fs.readFile(fullpath, 'utf8', function(err, data) {
      if (err) {
        res.status(500).send('Error reading rendered page');
      }
      res.status(200).send(data);
    });

  } else {
    res.status(400).send('There is no rendered page ' + req.url);
  }
});


app.listen(4000, function () {
  console.log('Simplebuy render service listening on port 4000!');
});


